/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombermanmodell;

import modell.FieldUnit;
import modell.Player;
import modell.Prize;
import modell.SpecialPrize;

/**
 *
 * @author Toth.Attila
 */
public class BomberManModell {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        FieldUnit player = new Player(1,1);        
        FieldUnit prize = new Prize(5,2,4);  
        FieldUnit specialPrize = new SpecialPrize();
        FieldUnit[] field = new FieldUnit[3];
        field[0] = player;
        field[1] = prize;
        field[2] = specialPrize;
        
        ((Prize) field[1]).getValue();
        
        for(FieldUnit unit : field){
            unit.doMyThing();
        }
    }
    
    public static void doEveryOneThing(FieldUnit fieldUnit){
        
    }
    
}
