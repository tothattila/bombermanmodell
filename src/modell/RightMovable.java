/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modell;

/**
 *
 * @author Toth.Attila
 */
public interface RightMovable extends Movable{
    void rightMovable();

    @Override
    public default void move(){
        System.out.println("I am moving right");
    }
}
