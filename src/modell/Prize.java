/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modell;

/**
 *
 * @author Toth.Attila
 */
public class Prize extends FieldUnit implements Speakable{
    public int value;

    public Prize() {
        System.out.println("Prize constructor without parameters");
    }

    public Prize(int value) {
        System.out.println("Prize constructor with parameters");

        this.value = value;
    }

    public Prize(int value, int x, int y) {
        super(x, y);
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void speak() {
        System.out.println("Prize speaking");
    }
    
    
    
}
