/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modell;

/**
 *
 * @author Toth.Attila
 */
public class FieldUnit {
    protected int x;
    
    protected int y;
    
    public void doMyThing(){
        System.out.println("Just doing my casual thing because I am a FieldUnit");

    }

    public FieldUnit() {
        System.out.println("FieldUnit constructor without parameters");
    }

    public FieldUnit(int x, int y) {
        System.out.println("FieldUnit constructor with parameters");
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    
    
    
}
