/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modell;

/**
 *
 * @author Toth.Attila
 */
public class Player extends FieldUnit implements LeftMovable, RightMovable {
    public void putBomb(int x, int y){}

    public Player() {
        System.out.println("Player constructor without parameters");
    }

    @Override
    public void doMyThing() {
        System.out.println("I am doing much more because I am a player which is much better than being a FieldUnit");
    }

    public Player(int x, int y) {
        super(x, y);
        System.out.println("Player constructor with parameters");
    }

    @Override
    public void leftMovable() {
        System.out.println("Moving left with leftmovable");
    }

    @Override
    public void rightMovable() {
        System.out.println("Moving right with rightmovable");
    }

    
    @Override
    public void move() {
        System.out.println("Just moving");
    }
    
}
